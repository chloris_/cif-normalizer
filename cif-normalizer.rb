#! /usr/bin/ruby

require "optparse"


# Constants

# Script name
NAME = "cif-normalizer"
# Script version
VERSION = "1.0.1"
# Script date
DATE = "2021-03-24"

# Maximum file size to read [bytes]
MAX_FILE_SIZE = 1_000_000

# Atoms begin token
ATOM_BEGIN_TOKEN = "_atom_site_"

# Parse stage enum
module ParseStage
    # Looking for tokens that are followed by atom data
    PREAMBLE = 0
    # Skipping tokens, nothing else should than atoms should follow
    ATOM_BEGIN_TOKEN = 1
    # Parse atom data
    ATOM_PARSE = 2
    # Atom data parsed, end
    END_PARSE = 3
end


# Classes

# Represents data parsed from atom site line.
class Atom
    # Read/write access to all fields
    attr_accessor :label, :type, :fract_x, :fract_y, :fract_z, :trailing

    # Initializes the fields to their default values.
    def initialize
        # Atom label
        @label = ""
        # Atom type
        @type = ""
        # Fractional coordinate x
        @fract_x = 0.0
        # Fractional coordinate y
        @fract_y = 0.0
        # Fractional coordinate z
        @fract_z = 0.0
        # Trailing data not used by the script
        @trailing = []
    end

    # Tries to parse atom data from an atom site data line. Returns an `Atom`
    # object if successful or `nil` otherwise.
    def Atom.parse_from_atom_line(line)
        split = line.split(" ")
        return nil if split.count < 5

        atom = Atom.new
        atom.label = split.shift
        atom.type = split.shift

        # Parse coordinates (may fail)
        begin
            atom.fract_x = atom.parse_coordinate(split.shift)
            atom.fract_y = atom.parse_coordinate(split.shift)
            atom.fract_z = atom.parse_coordinate(split.shift)
        rescue ArgumentError => e
            STDERR.puts "Error while parsing atom coordinates in line '#{line}':\n#{e}"
            return nil
        end

        atom.trailing = split
        return atom
    end

    # Parses given string `coordinate` into `Float` and returns it. If trailing
    # parentheses (`"(<any character sequence)"`) are found, the sequence along
    # with parentheses is ignored.
    #
    # Parse to `Float` is unguarded so this method raises the same exceptions
    # as `Float()`.
    def parse_coordinate(coordinate)
        if coordinate.end_with?(")")
            i = coordinate.rindex("(") - 1
            if not i.nil? and i >= 0
                coordinate = coordinate[..i]
            end
        end

        return Float(coordinate)
    end

    # Normalizes the fractional coordinates of the atom so that all of them
    # are between 0 and 1.
    def normalize!
        @fract_x = normalize_coordinate(@fract_x)
        @fract_y = normalize_coordinate(@fract_y)
        @fract_z = normalize_coordinate(@fract_z)
    end

    # Returns a normalized coordinate with value between 0.0 and 1.0. It is
    # obtained by adding or subtracting whole numbers (periodicity).
    def normalize_coordinate(x)
        while x > 1.0
            x -= 1.0
        end
        while x < 0.0
            x += 1.0
        end
        return x
    end

    # Returns an atom site data line from the contained data that can be
    # written back to the CIF file.
    def to_s
        trailing = ""
        unless trailing.empty?
            trailing = " #{trailing.join(" ")}"
        end

        return "%-5s %-3s %10.6f %10.6f %10.6f%s" % [label, @type, @fract_x, @fract_y, @fract_z, trailing]
    end
end


# Command line parameters

# Overwrite source file with result if true
opt_overwrite = false
# Output file path
opt_output = ""
# Input file path
opt_input = ""


# Display greeting
puts "#{NAME} version #{VERSION} (#{DATE})\n\n"


# Parse command line parameters

parser = OptionParser.new do |opts|
    opts.banner = "Usage: cif-normalizer.rb [options] INPUT_FILE"

    opts.on("-w", "--overwrite", "Overwrite source file with the result") do
        opt_overwrite = true
    end
    opts.on("-o PATH", "--output PATH", String, "Path to write resulting CIF file to") do |o|
        opt_output = o
    end
end

begin
    parser.parse!
rescue OptionParser::MissingArgument => e
    STDERR.puts "Missing argument error:\n#{e}"
    exit(-1)
rescue OptionParser::InvalidOption => e
    STDERR.puts "Invalid option error:\n#{e}"
    exit(-1)
rescue OptionParser => e
    STDERR.puts "Error while parsing arguments:\n#{e}"
    exit(-1)
end

opt_input = ARGV.shift
if opt_input.nil?
    STDERR.puts "No input file passed"
    exit(-1)
end
unless ARGV.empty?
    STDERR.puts "Too many arguments given (#{ARGV.join(", ")})"
    exit(-1)
end


# Evaluate command line arguments

if opt_output.empty? and not opt_overwrite
    STDERR.puts "No output file specified (parameter -o)"
    exit(-1)
end

if opt_overwrite and not opt_output.empty?
    STDERR.puts "Output file is redundant when overwrite is enabled"
    exit(-1)
end


# Read input file

puts ":: Reading input file '#{opt_input}'"

begin
    size = File.size(opt_input)
    if size > MAX_FILE_SIZE
        STDERR.puts "File '#{opt_input}' is too big (#{size})"
        exit(-1)
    end

    input_lines = File.readlines(opt_input)
rescue IOError => e
    STDERR.puts "Error while reading file: #{e}"
    exit(-1)
rescue SystemCallError => e
    STDERR.puts "File '#{opt_input}' does not exist."
    exit(-1)
end


# Process the input

puts ":: Processing input"

parse_stage = ParseStage::PREAMBLE
output_lines = []

input_lines.each do |line|
    line.strip!
    if line.empty?
        output_lines.push("")
        next
    end

    case parse_stage
    when ParseStage::PREAMBLE
        if line.start_with?(ATOM_BEGIN_TOKEN)
            parse_stage = ParseStage::ATOM_BEGIN_TOKEN
        end
        output_lines.push(line)
    when ParseStage::ATOM_BEGIN_TOKEN
        if line.start_with?(ATOM_BEGIN_TOKEN)
            output_lines.push(line)
            next
        else
            parse_stage = ParseStage::ATOM_PARSE
            redo
        end
    when ParseStage::ATOM_PARSE
        atom = Atom.parse_from_atom_line(line)
        if atom.nil?
            parse_stage = ParseStage::END_PARSE
            redo
        else
            atom.normalize!
            output_lines.push(atom.to_s)
        end
    when ParseStage::END_PARSE
        output_lines.push(line)
    else
        raise "Unknown parse stage #{parse_stage}"
    end
end


# Write the result

if opt_overwrite
    puts ":: Overriding source file with processed data"
    opt_output = opt_input
else
    puts ":: Writing results to file '#{opt_output}"
end

begin
    File.open(opt_output, "w") do |file|
        output_lines.each do |line|
            file.puts line
        end
    end

    puts ":: Done"
rescue SystemCallError => e
    STDERR.puts "System error while writing results:\n#{e}"
rescue IOError => e
    STDERR.puts "Error while writing results:\n#{e}"
    exit(-1)
end