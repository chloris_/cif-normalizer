# cif-normalizer

A simple tool to derive a CIF file with all atomic coordinates between 0 and 1.
This will cause all atoms to appear inside the unit cell and is achieved by
applying the simple law of periodicity.


## Installation

`cif-normalizer` is a Ruby script and no particular installation is necessary.
A working installation of [Ruby interpreter](https://www.ruby-lang.org/)
is required, though.


## Usage

Run the script with option `-h` or `--help` for usage instructions.

To normalize CIF file `input.cif` and write processed data to `output.cif`,
use the following command:
``` bash
ruby cif-normalizer.rb -o output.cif input.cif
```

Similarly, to overwrite the input file with process data, use this command:
``` bash
ruby cif-normalizer.rb -w input.cif
```
